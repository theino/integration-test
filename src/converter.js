/**
 * Padding output to match 2 characters always.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
};

module.exports = {
    /** 
     * Converts the RGB values to a Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
    */
    /**
 * HEX to RGB conversion
 * @param {string} hex with 7-characters, first one '#'
 * @param {string} rgb 
 * @returns {[number, number, number]} "RED", "GREEN", "BLUE"
 * @throws Error for incorrect hex values
 */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },
    /**
 * RGB to HEX conversion
 * @param {string} hex with 7-characters, first one '#'
 * @param {string} rgb 
 * @returns {[number, number, number]} "RED", "GREEN", "BLUE"
 * @throws Error for incorrect hex values
 */
    hexToRgb: (hex) => {
        const raw = hex.replace('#', '');
        const rgb = ["", "", ""];
        try {
            if (raw.length === 6) {
                const red = parseInt(raw.slice(0,2), 16);
                const green = parseInt(raw.slice(2,4), 16);
                const blue = parseInt(raw.slice(4,6), 16);
                if (!isNaN(red) && !isNaN(green) && !isNaN(blue)) {
                    rgb[0] = red;
                    rgb[1] = green;
                    rgb[2] = blue;
                } else {
                    throw new Error('');
                }
            } else if (raw.length === 3) {
                const red = parseInt(raw.slice(0,1).repeat(2), 16);
                const green = parseInt(raw.slice(1,2).repeat(2), 16);
                const blue = parseInt(raw.slice(2,3).repeat(2), 16);
                if (!isNaN(red) && !isNaN(green) && !isNaN(blue)) {
                    rgb[0] = red;
                    rgb[1] = green;
                    rgb[2] = blue;
                } else {
                    throw new Error('there is NaN');
                }
            }
        } catch (err) {
            console.log(err);
            throw new Error('Incorrect hex value');
        }
        return rgb;
}};