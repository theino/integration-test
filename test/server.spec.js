const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const PORT = 3000;

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to HEX conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        it("returns status 200", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in HEX", (done) => {
            const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
            request(url, (error, response, body) => {
                expect(body).to.equal("#ff0000");
                done();
            });
        });
        it("returns the color in RGB", (done) => {
            const url = baseurl + "/hex-to-rgb?r=ff&g=00&b=00";
            request(url, (error, response, body) => {
                expect(body).to.equal("[255,0,0]");
                done();
            });
        });
    });
    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});